zathura-pdf-poppler (0.3.3-1) unstable; urgency=medium

  * New upstream version 0.3.3
  * debian/control:
    - Bump Standards-Version
    - Add appstream-util and desktop-file-utils to Build-Depends for tests
    - Bump zathura-dev BD

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 04 Aug 2024 11:16:10 +0200

zathura-pdf-poppler (0.3.2-1) unstable; urgency=medium

  * New upstream version 0.3.2
  * debian/control:
    - Bump Standards-Version
    - Bump zathura dependency
    - Use pkgconf

 -- Sebastian Ramacher <sramacher@debian.org>  Fri, 08 Dec 2023 19:06:37 +0100

zathura-pdf-poppler (0.3.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.
  * Apply multi-arch hints. + zathura-pdf-poppler: Add Multi-Arch: same.
  * Remove constraints unnecessary since buster

  [ Sebastian Ramacher ]
  * Fix watch file
  * New upstream version 0.3.1
  * debian/control:
    - Bump Standards-Version
    - Bump debhelper compat to 13
    - Bump zathura-dev BD

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 27 Nov 2022 17:41:26 +0100

zathura-pdf-poppler (0.3.0-1) unstable; urgency=medium

  * New upstream release
  * debian/: Bump debhelper compat to 12
  * debian/control: Bump Standards-Version
  * debian/copyright:
    - Bump copyright years
    - Document appstream data license

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 07 Jan 2020 21:27:04 +0100

zathura-pdf-poppler (0.2.9-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Bump Standards-Version.
  * debian/copyright: Bump copyright years.
  * debian/: Convert to meson build system.
  * debian/rules: Use zathura sequence.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 18 Mar 2018 12:45:22 +0100

zathura-pdf-poppler (0.2.8-1) unstable; urgency=medium

  * New upstream release.
  * debian/: Bump debhelper compat to 11.
  * debian/control:
    - Bump B-Ds.
    - Bump Standards-Version.
    - Remove obsolete Breaks+Replaces.
    - Move to salsa.debian.org.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 24 Dec 2017 16:50:24 +0100

zathura-pdf-poppler (0.2.7-1) unstable; urgency=medium

  * New upstream release.
    - Sort source files to make build reproducible. (Closes: #842986)
  * debian/{control,rules,control}: Bump debhelper compat to 10.
  * debian/control:
    - Bump Standards-Version.
    - Update Vcs-Git.

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 11 Jan 2017 23:23:55 +0100

zathura-pdf-poppler (0.2.6-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release.
  * debian/control: Recommend poppler-data.

 -- Sebastian Ramacher <sramacher@debian.org>  Fri, 22 Jan 2016 01:05:50 +0100

zathura-pdf-poppler (0.2.5-1) experimental; urgency=medium

  * Split zathura-pdf-poppler from zathura.
    While 3.0 (quilt) multi tarball packages are nice, they do not integrate
    well with git-buildpackage.

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 21 Dec 2015 20:00:12 +0100
